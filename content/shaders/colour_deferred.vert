#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

uniform mat4 modelMatrix = mat4(1.0);
uniform mat4 combinedMatrix;

out vec3 normal;
out vec3 position;

void main()
{
	position = vec3( modelMatrix * vec4( vertexPosition, 1.0 ) );
	normal = vec3( transpose( inverse( modelMatrix ) ) *
      vec4( vertexNormal,0.0 ) );
  normal = normalize( normal );
	gl_Position = combinedMatrix * vec4( vertexPosition, 1.0 );
}
