#version 330

in vec3 normal;
in vec3 position;
in vec2 textureCoordinate;

uniform sampler2D diffuseTexture;

out vec4[4] output;

void main()
{
	vec4 colour = texture2D( diffuseTexture, textureCoordinate.st ).rgba;
  if ( colour.a > 0.1 )
  {
    output[0] = colour;
    output[1] = vec4( position, 1 );
    output[2] = vec4( normal, 1 );
  }
  else
  {
    discard;
  }
}
