#version 330

uniform sampler2DMS depthTexture;
uniform int numSamples;
uniform vec2 windowDimensions;
uniform mat4 invViewProjection;
uniform mat4 invModelMatrix;
uniform sampler2D diffuseTexture;

in vec2 textureCoordinates;

out vec4 output;

float LinearizeDepth( float z, float near, float far )
{
  return ( 2.0 * near ) / ( far + near - z * ( far - near ) );
}
void main()
{
  vec2 screenPosition = gl_FragCoord.xy / windowDimensions;
  vec2 ndc = screenPosition * 2.0 - vec2( 1.0 );

  ivec2 texelCoord = ivec2( textureSize( depthTexture ) * screenPosition );
  float depth = 0.0;
  for ( int i = 0; i < numSamples; ++i )
  {
    depth += texelFetch( depthTexture, texelCoord, i ).r;
  }
  depth /= numSamples;
  depth = depth * 2.0 - 1.0;

  vec4 worldPosition = invViewProjection * vec4( ndc, depth, 1.0 );
  worldPosition.xyz /= worldPosition.w;
  vec3 position = vec3( invModelMatrix * vec4( worldPosition.xyz, 1.0 ) );

  if ( ( abs( position.x ) > 0.5 ) || ( abs( position.y ) > 0.5 ) ||
       ( abs( position.z ) > 0.5 ) )
  {
    discard;
  }
  vec2 decalTextureCoordinates = position.xy + 0.5;
  output = texture( diffuseTexture, decalTextureCoordinates ).rgba;
}
