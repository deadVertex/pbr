#version 330

#define UNKNOWN 0
#define DIFFUSE_COLOUR 1
#define POSITIONS 2
#define NORMALS 3
#define SHADED 4
#define DEPTH 5
#define SURFACE 6

#define M_PI 3.1415926535897932384626433832795

in vec2 textureCoordinates;

uniform sampler2DMS diffuseTexture;
uniform sampler2DMS surfaceTexture;
uniform sampler2DMS normalsTexture;
uniform sampler2DMS depthTexture;
uniform int outputType;
uniform int numSamples;

uniform sampler2DShadow shadowTexture;
uniform mat4 shadowViewProjection;

uniform vec3 cameraPosition;
uniform samplerCube reflectionTexture;
uniform samplerCube irradianceTexture;

uniform mat4 invViewProjection;

out vec4 output;
vec2 poissonDisk[16] = vec2[](
    vec2( -0.94201624, -0.39906216 ),
    vec2( 0.94558609, -0.76890725 ),
    vec2( -0.094184101, -0.92938870 ),
    vec2( 0.34495938, 0.29387760 ),
    vec2( -0.91588581, 0.45771432 ),
    vec2( -0.81544232, -0.87912464 ),
    vec2( -0.38277543, 0.27676845 ),
    vec2( 0.97484398, 0.75648379 ),
    vec2( 0.44323325, -0.97511554 ),
    vec2( 0.53742981, -0.47373420 ),
    vec2( -0.26496911, -0.41893023 ),
    vec2( 0.79197514, 0.19090188 ),
    vec2( -0.24188840, 0.99706507 ),
    vec2( -0.81409955, 0.91437590 ),
    vec2( 0.19984126, 0.78641367 ),
    vec2( 0.14383161, -0.14100790 )
);

float random( vec4 seed4 )
{
  float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
  return fract(sin(dot_product) * 43758.5453);
}

// NOTE: This a return value of 1 means no shadow.
float CalculateShadowAmount( vec3 position )
{
  vec4 p = shadowViewProjection * vec4( position, 1 );
  p /= p.w;
  vec3 coords = ( p.xyz ) * 0.5 + vec3( 0.5 );
  //coords.z -= 0.001;
#if 0
  return texture( shadowTexture, coords, 0.0 );
#endif
  float result = 1.0;
  vec3 shadowCoords;
  float bias = 0.005;
  for ( int i = 0; i < 4; i++ )
  {
    // Use for noisy soft shadows.
    //int index = int( 16.0 * random( vec4( position.xyz * 1000.0, i ) ) ) % 16;
    shadowCoords.xy = coords.xy + poissonDisk[i] / 700.0;
    shadowCoords.z = coords.z - bias;
    result -= 0.25 * ( 1.0 - texture( shadowTexture, shadowCoords ) );
  }
  return result;
}

vec3 LambertianBrdf( vec3 colourDiffuse )
{
  return colourDiffuse / M_PI;
}

vec3 Fresnel_Schlick( vec3 F0, float cosT, float power )
{
  return F0 + ( vec3( 1 ) - F0 ) * pow( 1 - cosT, power );
}

vec3 BlinnPhongBrdf( vec3 L, vec3 N, vec3 V, vec3 H, vec3 colourDiffuse,
                     vec3 colourSpec, float roughness, vec3 F0 )
{
  float specAngle = max( 0.0, dot( H, N ) );
  float specular = pow( specAngle, roughness );
  float specMul = ( roughness + 8 ) / ( 8 * M_PI );
  float cosAlpha = max( 0.0, dot( H, L ) );
  return LambertianBrdf( colourDiffuse ) +
         specMul * Fresnel_Schlick( F0, cosAlpha, 5 ) * specular;
}

float ComputeGeometric( vec3 N, vec3 H, vec3 V )
{
  float nDotV = max( 0.0, dot( N, V ) );
  float nDotH = max( 0.0, dot( N, H ) );
  float vDotH = max( 0.0, dot( V, H ) );
  return min( 1, ( 2 * nDotH * nDotV ) / vDotH );
}

float BeckmannDistribution( vec3 N, vec3 H, float m )
{
  float nDotH = max( 0.0001, dot( N, H ) );
  float a = 1.0f / ( 4.0f * m * m * pow( nDotH, 4 ) );
  float b = nDotH * nDotH - 1.0f;
  float c = m * m * nDotH * nDotH;

  return a * exp( b / c );
}

vec3 CookTorranceBrdf( vec3 L, vec3 N, vec3 V, vec3 H, vec3 colourSpec,
                       float roughness )
{
  float facing = clamp( dot( V, N ), 0.0, 1.0 );
  vec3 fresnel = Fresnel_Schlick( colourSpec, facing, 5 );
  float roughnessTerm = BeckmannDistribution( N, H, roughness );
  float geometric = ComputeGeometric( N, H, V );
  vec3 cookTorrance =
    ( fresnel * roughnessTerm * geometric ) /
    ( max( 0.0001, dot( N, V ) ) * max( 0.0001, dot( N, L ) ) );
  return colourSpec * cookTorrance;
}

vec3 lerp( vec3 a, vec3 b, float t )
{
  return a * ( 1 - t ) + t * b;
}
vec3 TestBrdf( vec3 N, vec3 V, vec3 colourSpec, float roughness,
               float metallic )
{
  vec3 R = normalize( 2 * dot( N, V ) * N - V );
  vec3 reflection = textureLod( reflectionTexture, R, roughness * 6.0 ).rgb;
  float cosTheta = clamp( dot( N, V ), 0.0, 1.0 );
  vec3 reflectionColour = lerp( vec3( 1 ), colourSpec, metallic );
  return Fresnel_Schlick( colourSpec, cosTheta, 5 ) * reflection * reflectionColour;
}

vec3 PhysicallyBasedDirectionalLighting( vec3 L, vec3 N, vec3 V,
                                         vec3 lightIrradiance,
                                         vec3 colourDiffuse, vec3 colourSpec,
                                         float roughness, vec3 position )
{
  float cosTheta = max( 0.0, dot( N, L ) );
  vec3 H = normalize( V + L );
  return ( LambertianBrdf( colourDiffuse ) +
           CookTorranceBrdf( L, N, V, H, colourSpec, roughness ) ) *
         lightIrradiance * cosTheta * CalculateShadowAmount( position );
}

vec3 PhysicallyBasedHemiLighting( vec3 L, vec3 N, vec3 V, vec3 lightIrradiance,
                                  vec3 colourDiffuse, vec3 colourSpec )
{
	float cosTheta = ( dot( N, L) * 0.5 ) + 0.5;
  return LambertianBrdf( colourDiffuse ) * lightIrradiance * cosTheta;
}

float LinearizeDepth( float z, float near, float far )
{
  return ( 2.0 * near ) / ( far + near - z * ( far - near ) );
}

vec3 LightPixel( vec3 diffuseColour, vec3 position, vec3 normal,
                 vec3 surfaceProperties, float depth )
{
  float metallic = surfaceProperties.r;
  vec3 specularColour = vec3( 0.04 );
  specularColour = lerp( specularColour, diffuseColour, metallic );
  diffuseColour = lerp( diffuseColour, vec3( 0.0 ), metallic );
  float roughness = surfaceProperties.g;
  vec3 sunDir = -normalize( vec3( -0.2, -1, -0.8 ) );
  vec3 sunColour = vec3( 1.0 ) * 1.0;
  vec3 result = vec3( 0 );

  normal = normalize( normal );
  vec3 view = normalize( cameraPosition - position );

  result += TestBrdf( normal, view, specularColour, roughness, metallic );

  if ( metallic < 1.0 )
  {
    result += diffuseColour * texture( irradianceTexture, normal ).rgb;

    result += PhysicallyBasedDirectionalLighting(
      sunDir, normal, view, sunColour, diffuseColour, specularColour, roughness,
      position );
  }
  return result;
}

vec4 ProcessSample( vec3 diffuseColour, vec3 position, vec3 normal,
                    vec3 surfaceProperties, float depth, float alpha )
{
  if ( alpha < 0.1 )
  {
    return vec4( 0 );
  }
  switch ( outputType )
  {
  case SHADED:
    return vec4(
      LightPixel( diffuseColour, position, normal, surfaceProperties, depth ),
      1.0 );

  case DIFFUSE_COLOUR:
    return vec4( diffuseColour, 1.0 );

  case POSITIONS:
    return vec4( position, 1.0 );

  case NORMALS:
    return vec4( normal, 1.0 );

  case DEPTH:
    // NOTE: This depends on the projection matrix used.
    depth = LinearizeDepth( depth, 0.1, 100.0 );
    return vec4( depth, depth, depth, 1.0 );

  case SURFACE:
    return vec4( surfaceProperties, 1.0 );

  default:
    return vec4( 1.0, 0.0, 0.0, 1.0 );
  }
}

vec3 CalculateWorldPosition( vec2 position, float depth )
{
  vec3 worldPosition = vec3( position, depth ) * 2.0 - 1.0;
  vec4 temp = invViewProjection * vec4( worldPosition, 1.0 );
  worldPosition = temp.xyz / temp.w;
  return worldPosition;
}

void main()
{
  ivec2 texelCoord =
    ivec2( textureSize( diffuseTexture ) * textureCoordinates.st );
  if ( numSamples > 0 )
  {
    output = vec4(0);
    for ( int i = 0; i < numSamples; ++i )
    {
      vec3 diffuseColour = texelFetch( diffuseTexture, texelCoord, i ).rgb;
      float alpha = texelFetch( diffuseTexture, texelCoord, i ).a;
      vec3 surfaceProperties = texelFetch( surfaceTexture, texelCoord, i ).rgb;
      vec3 normal = texelFetch( normalsTexture, texelCoord, i ).xyz;
      float depth = texelFetch( depthTexture, texelCoord, i ).r;
      vec3 position = CalculateWorldPosition( textureCoordinates.st, depth );
      output += ProcessSample( diffuseColour, position, normal,
                               surfaceProperties, depth, alpha );
    }
    output /= numSamples;
  }
  else
  {
    vec3 diffuseColour = texelFetch( diffuseTexture, texelCoord, 0 ).rgb;
    float alpha = texelFetch( diffuseTexture, texelCoord, 0 ).a;
    vec3 surfaceProperties = texelFetch( surfaceTexture, texelCoord, 0 ).rgb;
    vec3 normal = texelFetch( normalsTexture, texelCoord, 0 ).xyz;
    float depth = texelFetch( depthTexture, texelCoord, 0 ).r;
    vec3 position = CalculateWorldPosition( textureCoordinates.st, depth );

    output = ProcessSample( diffuseColour, position, normal, surfaceProperties,
                            depth, alpha );
  }
}
