#version 330

in vec3 normal;
centroid in vec2 textureCoordinate;

uniform sampler2D diffuseTexture;
uniform float metallic = 0.0;
uniform float roughness = 0.8;

out vec4[4] output;

void main()
{
	vec3 colour = texture2D( diffuseTexture, textureCoordinate.st ).rgb;
  output[0] = vec4( colour, 1 );
  output[1] = vec4( metallic, roughness, 0, 1 );
  output[2] = vec4( normal, 1 );
}
