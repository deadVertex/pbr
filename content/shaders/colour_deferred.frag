#version 330

in vec3 normal;

uniform vec4 colour;
uniform float metallic = 0.0;
uniform float roughness = 0.1;

out vec4[4] output;

void main()
{
  output[0] = colour;
  output[1] = vec4( metallic, roughness, 0, 1 );
  output[2] = vec4( normal, 1 );
}
