#version 330

layout( location = 0 ) in vec3 vertexPosition;
layout( location = 1 ) in vec3 vertexNormal;
layout( location = 2 ) in vec2 vertexTextureCoordinates;

uniform mat4 modelMatrix = mat4(1);
uniform mat4 combinedMatrix;

out vec2 textureCoordinates;

void main()
{
	textureCoordinates = vertexTextureCoordinates;
	gl_Position = combinedMatrix * vec4( vertexPosition, 1.0 );
}
