#version 330

in vec3 textureCoordinates;

uniform samplerCube cubeTexture;

out vec4 output;

void main()
{
  output = textureLod( cubeTexture, textureCoordinates, 0.0 );
}
