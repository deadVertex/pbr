#version 330

layout(location = 0) in vec3 vertexPosition;

uniform mat4 combinedMatrix;
uniform mat4 viewMatrix;

out vec3 textureCoordinates;

void main()
{
  textureCoordinates = vertexPosition;
  gl_Position = combinedMatrix * vec4( vertexPosition, 1.0 );
}
