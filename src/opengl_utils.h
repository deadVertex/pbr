#pragma once

#include <cstdint>

#define GLEW_STATIC
#include <GL/glew.h>

#define INVALID_OPENGL_SHADER 0
#define INVALID_OPENGL_TEXTURE 0

enum
{
  VERTEX_ATTRIBUTE_POSITION = 0,
  VERTEX_ATTRIBUTE_NORMAL = 1,
  VERTEX_ATTRIBUTE_TEXTURE_COORDINATE = 2,
  VERTEX_ATTRIBUTE_COLOUR = 3,
  MAX_VERTEX_ATTRIBUTES,
};

struct OpenGLStaticMesh
{
  uint32_t vao, vbo, ibo, numIndices, numVertices, primitive, indexType;
};

typedef void ( *OpenGLErrorMessageCallback )( const char * );

extern void OpenGLDeleteShader( uint32_t program );

extern uint32_t OpenGLCreateShader( const char *vertSource, uint32_t vertLength,
                                    const char *fragSource, uint32_t fragLength,
                                    OpenGLErrorMessageCallback callback );

struct OpenGLVertexAttribute
{
  GLuint index;
  GLint numComponents;
  GLenum componentType;
  GLboolean normalized;
  GLsizei offset;
};

extern void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, GLenum primitive );
extern void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh CreateWireframeCube();
extern OpenGLStaticMesh CreateAxisMesh();
extern OpenGLStaticMesh CreateFullscreenQuadMesh();
extern OpenGLStaticMesh CreatePlaneMesh( float width = 1.0f,
                                         float height = 1.0f,
                                         float textureScale = 1.0f );
extern void OpenGLDeleteTexture( uint32_t texture );
extern uint32_t OpenGLCreateTexture( uint32_t width, uint32_t height,
                                     int pixelFormat, int pixelComponentType,
                                     const void *pixels );

extern OpenGLStaticMesh CreateCubeMesh();
