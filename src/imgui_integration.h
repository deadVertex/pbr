/*
 * Most of this code was copied from the ImGUI opengl3 demo and modified to
 * work with my code for input handling and rendering.
 */

// ImGui GLFW binding with OpenGL3 + shaders
// https://github.com/ocornut/imgui

struct GLFWwindow;

bool uiInit( GLFWwindow *window );
void uiShutdown();
void uiNewFrame( float mouseX, float mouseY, double currentTime, float dt );

void uiOnMouseButtonInput( GLFWwindow *window, int button, int action,
                           int mods );
void uiOnScroll( GLFWwindow *window, double xoffset, double yoffset );
void uiOnKeyInput( GLFWwindow *window, int key, int scancode, int action,
                   int mods );
void uiOnCharInput( GLFWwindow *window, unsigned int c );
