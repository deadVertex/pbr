#include <cstdio>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/compatibility.hpp>

#include "opengl_utils.h"
#include "render.h"
#include "utils.h"
#include "imgui.h"
#include "imgui_integration.h"

static bool run = true;
static glm::mat4 projectionMatrix;
static bool enableMouseLook = false;
static int windowWidth, windowHeight;
static bool windowWasResized = true;
static void WindowResizeCallback( GLFWwindow *window, int width, int height )
{
  projectionMatrix = glm::perspective( glm::radians( 80.0f ),
      (float)width / (float)height, 0.1f, 100.0f );
  windowWidth = width;
  windowHeight = height;
  windowWasResized = true;
}

static void UpdateInputMode( GLFWwindow *window, bool useCursor )
{
  if ( useCursor )
  {
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
  }
  else
  {
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
  }
}

enum
{
  KEY_W = 1,
  KEY_S = 2,
  KEY_A = 4,
  KEY_D = 8
};

uint8_t keyStates = 0;
static void KeyCallback( GLFWwindow* window, int key, int scancode, int action,
                         int mods )
{
  if ( !enableMouseLook )
  {
    uiOnKeyInput( window, key, scancode, action, mods );
  }
  if ( action == GLFW_PRESS )
  {
    switch ( key )
    {
    case GLFW_KEY_W:
      keyStates |= KEY_W;
      break;
    case GLFW_KEY_S:
      keyStates |= KEY_S;
      break;
    case GLFW_KEY_A:
      keyStates |= KEY_A;
      break;
    case GLFW_KEY_D:
      keyStates |= KEY_D;
      break;
    case GLFW_KEY_ESCAPE:
      run = false;
      break;
    default:
      break;
    }
  }
  else if ( action == GLFW_RELEASE )
  {
    switch ( key )
    {
    case GLFW_KEY_W:
      keyStates &= ~KEY_W;
      break;
    case GLFW_KEY_S:
      keyStates &= ~KEY_S;
      break;
    case GLFW_KEY_A:
      keyStates &= ~KEY_A;
      break;
    case GLFW_KEY_D:
      keyStates &= ~KEY_D;
      break;
    default:
      break;
    }
  }
}

void MouseButtonCallback( GLFWwindow *window, int button, int action, int mods )
{
  if ( !enableMouseLook )
  {
    uiOnMouseButtonInput( window, button, action, mods );
  }

  if ( button == GLFW_MOUSE_BUTTON_RIGHT )
  {
    if ( action == GLFW_PRESS )
    {
      glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
      enableMouseLook = true;
    }
    else
    {
      glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
      enableMouseLook = false;
    }
  }
}

struct Camera
{
  glm::vec3 position, orientation;
  glm::vec3 oldPosition, oldOrientation;
};

static void UpdateCamera( Camera* camera, glm::vec2 rotation,
                          glm::vec3 velocity, float dt )
{
  camera->oldPosition = camera->position;
  camera->oldOrientation = camera->orientation;

  camera->orientation += glm::vec3( rotation, 0 ) * dt;
  camera->orientation.x = glm::clamp(
    camera->orientation.x, -glm::half_pi<float>(), glm::half_pi<float>() );

  glm::mat4 rotationMatrix;
  rotationMatrix =
    glm::rotate( rotationMatrix, camera->orientation.x, glm::vec3{1, 0, 0} );
  rotationMatrix =
    glm::rotate( rotationMatrix, camera->orientation.y, glm::vec3{0, 1, 0} );

  rotationMatrix = glm::inverse( rotationMatrix );
  glm::vec3 forward = glm::vec3( rotationMatrix * glm::vec4{0, 0, 1, 0} );
  forward = glm::normalize( forward );

  glm::vec3 right = glm::vec3( rotationMatrix * glm::vec4{1, 0, 0, 0} );
  right = glm::normalize( right );

  glm::vec3 up = glm::vec3( rotationMatrix * glm::vec4{0, 1, 0, 0} );
  up = glm::normalize( up );

  camera->position += right * velocity.x * dt;
  camera->position += forward * velocity.z * dt;
}

static glm::mat4 CreateViewMatrix( Camera *camera, float interp )
{
  glm::vec3 position =
    glm::lerp( camera->oldPosition, camera->position, interp );
  glm::vec3 orientation =
    glm::lerp( camera->oldOrientation, camera->orientation, interp );

  glm::mat4 viewMatrix;
  viewMatrix =
    glm::rotate( viewMatrix, orientation.x, glm::vec3{1, 0, 0} );
  viewMatrix =
    glm::rotate( viewMatrix, orientation.y, glm::vec3{0, 1, 0} );
  viewMatrix = glm::translate( viewMatrix, -position );
  return viewMatrix;
}

void APIENTRY OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                                        GLenum severity, GLsizei length,
                                        const GLchar *message,
                                        const void *userParam )
{
  const char *typeStr = nullptr;
  switch ( type )
  {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "ERROR";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "DEPRECATED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "UNDEFINED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "PORTABILITY";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "PERFORMANCE";
      break;
    case GL_DEBUG_TYPE_OTHER:
      typeStr = "OTHER";
      break;
    default:
      typeStr = "";
      break;
  }

  const char *severityStr = nullptr;
  switch ( severity )
  {
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "LOW";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "MEDIUM";
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "HIGH";
      break;
    default:
      severityStr = "";
      break;
  }

  fprintf( stdout, "OPENGL|%s:%s:%s\n", typeStr, severityStr, message );
}

struct Scene
{
  Shader colourOnlyShader, vertexColourOnlyShader, colourDeferred,
    textureDeferred, textureDeferredStencil, decalShader, cubemapShader;
  GBuffer gbuffer;
  OpenGLStaticMesh fullscreenQuad, wireframeCube, axisMesh, monkeyMesh,
    groundPlaneMesh, cubeMesh, levelGeometryMesh;
  uint32_t devTileTexture, fenceTexture, bulletHoleTexture, skyboxTexture,
    irradianceTexture, reflectionTexture;
  ShadowMap shadowMap;
  Camera camera;
  glm::vec3 colour;
  float metallic, roughness;
};

internal void LoadContent( Scene *scene )
{
  scene->colourOnlyShader = LoadShader( "../content/shaders/colour_only.msh" );
  scene->vertexColourOnlyShader =
    LoadShader( "../content/shaders/vertex_colour_only.msh" );
  scene->gbuffer.shader = LoadShader( "../content/shaders/gbuffer.vert",
                                      "../content/shaders/gbuffer.frag" );
  scene->colourDeferred =
    LoadShader( "../content/shaders/colour_deferred.vert",
                "../content/shaders/colour_deferred.frag" );
  scene->textureDeferred =
    LoadShader( "../content/shaders/texture_deferred.vert",
                "../content/shaders/texture_deferred.frag" );
  scene->textureDeferredStencil =
    LoadShader( "../content/shaders/texture_deferred_alpha.vert",
                "../content/shaders/texture_deferred_alpha.frag" );
  scene->decalShader = LoadShader( "../content/shaders/decal.vert",
                                   "../content/shaders/decal.frag" );
  scene->cubemapShader = LoadShader( "../content/shaders/cubemap_shader.vert",
                                     "../content/shaders/cubemap_shader.frag" );

  scene->fullscreenQuad = CreateFullscreenQuadMesh();
  scene->wireframeCube = CreateWireframeCube();
  scene->axisMesh = CreateAxisMesh();
  scene->monkeyMesh = LoadMesh( "../content/meshes/monkey.mme" );
  scene->groundPlaneMesh = CreatePlaneMesh( 4.0f, 4.0f, 2.0f );
  scene->cubeMesh = CreateCubeMesh();
  scene->devTileTexture = LoadTexture( "../content/textures/dev_tile512.mte" );
  scene->fenceTexture = LoadTexture( "../content/textures/fence.mte" );
  scene->bulletHoleTexture = LoadTexture( "../content/textures/bullethole.mte" );
#if 0
  scene->skyboxTexture = LoadCubeMap(
    "../content/textures/negz.jpg", "../content/textures/posz.jpg",
    "../content/textures/posy.jpg", "../content/textures/negy.jpg",
    "../content/textures/negx.jpg", "../content/textures/posx.jpg" );
#endif
  scene->skyboxTexture =
    LoadCubeMapCross( "../content/textures/desert_reflection2.tga" );
  scene->irradianceTexture =
    LoadCubeMapCross( "../content/textures/desert_irradiance.tga" );

  const char *reflectionTextures[] = {
    "../content/textures/desert_reflection0.tga",
    "../content/textures/desert_reflection1.tga",
    "../content/textures/desert_reflection2.tga",
    "../content/textures/desert_reflection3.tga",
    "../content/textures/desert_reflection4.tga",
    "../content/textures/desert_reflection5.tga",
    "../content/textures/desert_reflection6.tga",
    NULL};

  scene->reflectionTexture =
    LoadCubeMapCrossLOD( reflectionTextures );

  InitializeShadowMap( &scene->shadowMap );
  scene->shadowMap.viewMatrix =
    glm::rotate( glm::mat4(), glm::quarter_pi<float>(), glm::vec3{1, 0, 0} );
}

internal void RenderScene( Scene *scene, const glm::mat4 &viewProjection )
{
  glUseProgram( scene->colourDeferred.program );
  glm::vec4 colour{ scene->colour, 1 };
  glUniform4fv( scene->colourDeferred.colourLocation, 1,
                glm::value_ptr( colour ) );
  glUniform1f( scene->colourDeferred.metallicLocation, scene->metallic );
  glUniform1f( scene->colourDeferred.roughnessLocation, scene->roughness );
  glm::mat4 monkeyModelMatrix =
    glm::translate( glm::mat4(), glm::vec3{0, 1, 0} );
  auto monkeyCombined = viewProjection * monkeyModelMatrix;
  glUniformMatrix4fv( scene->colourDeferred.combinedMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( monkeyCombined ) );
  glUniformMatrix4fv( scene->colourDeferred.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( monkeyModelMatrix ) );
  OpenGLDrawStaticMesh( scene->monkeyMesh );

  glm::mat4 identity;
  glUseProgram( scene->textureDeferred.program );
  glUniformMatrix4fv( scene->textureDeferred.combinedMatrixLocation, 1,
      GL_FALSE, glm::value_ptr( viewProjection ) );
  glUniformMatrix4fv( scene->textureDeferred.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( identity ) );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, scene->devTileTexture );
  glUniform1i( scene->textureDeferred.diffuseTextureLocation, 0 );
  OpenGLDrawStaticMesh( scene->groundPlaneMesh );

#if 0
  glUseProgram( scene->textureDeferred.program );
  glUniformMatrix4fv( scene->textureDeferred.combinedMatrixLocation, 1,
      GL_FALSE, glm::value_ptr( viewProjection ) );
  glUniformMatrix4fv( scene->textureDeferred.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( identity ) );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, scene->devTileTexture );
  glUniform1i( scene->textureDeferred.diffuseTextureLocation, 0 );
  OpenGLDrawStaticMesh( scene->levelGeometryMesh );
#endif
}

internal void Render( Scene *scene, float interp )
{
  glViewport( 0, 0, windowWidth, windowHeight );
  glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_TEXTURE_2D );
  glEnable( GL_FRAMEBUFFER_SRGB );
  glEnable( GL_TEXTURE_CUBE_MAP_SEAMLESS );

  if ( scene->shadowMap.fbo )
  {
    glBindFramebuffer( GL_FRAMEBUFFER, scene->shadowMap.fbo );
    glViewport( 0, 0, scene->shadowMap.width, scene->shadowMap.height );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable( GL_DEPTH_TEST );
    glDrawBuffers( 1, &scene->shadowMap.renderTarget );

    glm::mat4 shadowViewProjection =
      scene->shadowMap.projectionMatrix * scene->shadowMap.viewMatrix;

    RenderScene( scene, shadowViewProjection );
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  }
  if ( scene->gbuffer.fbo )
  {
    glBindFramebuffer( GL_FRAMEBUFFER, scene->gbuffer.fbo );
    glViewport( 0, 0, windowWidth, windowHeight );
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glDrawBuffers( 3, scene->gbuffer.renderTargets );

    glm::mat4 viewMatrix;
    viewMatrix = CreateViewMatrix( &scene->camera, interp );
    glm::mat4 viewProjection = projectionMatrix * viewMatrix;

    RenderScene( scene, viewProjection );

    // Do decal pass.

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glViewport( 0, 0, windowWidth, windowHeight );
    glEnable( GL_TEXTURE_2D );
    glEnable( GL_BLEND );
    glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
    glDisable( GL_DEPTH_TEST );

    glm::vec3 cameraOrientation = glm::lerp(
      scene->camera.oldOrientation, scene->camera.orientation, interp );

    glm::mat4 skyboxViewMatrix = glm::rotate(
      glm::mat4(), cameraOrientation.x, glm::vec3{1, 0, 0} );
    skyboxViewMatrix = glm::rotate( skyboxViewMatrix, cameraOrientation.y,
                                    glm::vec3{0, 1, 0} );
    glUseProgram( scene->cubemapShader.program );
    auto skyboxCombined = projectionMatrix * skyboxViewMatrix;
    glUniformMatrix4fv( scene->cubemapShader.combinedMatrixLocation, 1, GL_FALSE,
        glm::value_ptr( skyboxCombined ) );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_CUBE_MAP, scene->skyboxTexture );
    glUniform1i( scene->cubemapShader.cubemapLocation, 0 );
    glEnable( GL_CULL_FACE );
    glCullFace( GL_FRONT );
    OpenGLDrawStaticMesh( scene->cubeMesh );
    glDisable( GL_CULL_FACE );

    glm::mat4 invViewProjection = glm::inverse( projectionMatrix * viewMatrix );

    DisplayGBuffer( &scene->gbuffer, scene->fullscreenQuad, scene->shadowMap,
                    scene->camera.position, scene->reflectionTexture,
                    scene->irradianceTexture, invViewProjection );
  }
}

static void DisplayMaterialPropertiesWindow( Scene *scene )
{
  ImGui::ColorEdit3( "Colour", &scene->colour.r );
  ImGui::DragFloat( "Metallic", &scene->metallic, 0.01f, 0.0f, 1.0f );
  ImGui::DragFloat( "Roughness", &scene->roughness, 0.01f, 0.01f, 1.0f );
}

int main( int argc, char **argv )
{
  if ( !glfwInit() )
  {
    fprintf( stderr, "Failed to initialize GLFW.\n" );
    return -1;
  }
  //glfwWindowHint( GLFW_DOUBLEBUFFER, 1 );
  glfwWindowHint( GLFW_SAMPLES, 0 );
  glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
  GLFWwindow *window =
    glfwCreateWindow( 1280, 720, "Physically Based Rendering", NULL, NULL );
  windowWidth = 1280;
  windowHeight = 720;
  if ( !window )
  {
    glfwTerminate();
    fprintf( stderr, "Failed to create GLFW window.\n" );
    return -1;
  }
  WindowResizeCallback( window, windowWidth, windowHeight );

  glfwMakeContextCurrent( window );
  glfwSetWindowSizeCallback( window, WindowResizeCallback );
  glfwSetKeyCallback( window, KeyCallback );
  glfwSetMouseButtonCallback( window, MouseButtonCallback );
  glfwSetScrollCallback( window, uiOnScroll );
  glfwSetCharCallback( window, uiOnCharInput );
  glfwSwapInterval( 0 );

  GLenum err = glewInit();
  if ( err != GLEW_OK )
  {
    fprintf( stderr, "GLEW ERROR: %s\n", glewGetErrorString( err ) );
    return -1;
  }
  fprintf( stdout, "Using GLEW %s.\n", glewGetString( GLEW_VERSION ) );

  glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
  glDebugMessageCallback( OpenGLReportErrorMessage, nullptr );
  GLuint unusedIds = 0;
  glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                         &unusedIds, GL_TRUE );

  Scene scene;
  LoadContent( &scene );
  scene.colour.g = 0.9f;
  scene.roughness = 0.2f;
  uiInit( window );

  double t = 0.0;
  float logicHz = 30.0f;
  float logicTimestep = 1.0f / logicHz;
  double maxFrameTime = 0.25; // Used to prevent the simulation from being
                              // affected by break points.

  double currentTime = glfwGetTime();
  double accumulator = 0.0;

  double prevMouseX = 0.0, prevMouseY = 0.0;
  scene.camera.position = glm::vec3{ 0, 1, 4 };
  while ( run )
  {
    double newTime = glfwGetTime();
    double frameTime = newTime - currentTime;
    if ( frameTime > maxFrameTime )
      frameTime = maxFrameTime;
    currentTime = newTime;

    accumulator += frameTime;

    while ( accumulator >= logicTimestep )
    {
      glfwPollEvents();
      if ( glfwWindowShouldClose( window ) )
      {
        run = false;
      }
      if ( windowWasResized )
      {
        DeinitializeGBuffer( &scene.gbuffer );
        InitializeGBuffer( &scene.gbuffer, windowWidth, windowHeight );
        windowWasResized = false;
      }

      glm::vec2 rotation;
      glm::vec3 velocity;

      double mousePosX, mousePosY;
      glfwGetCursorPos( window, &mousePosX, &mousePosY );
      double dx = mousePosX - prevMouseX;
      double dy = mousePosY - prevMouseY;
      prevMouseX = mousePosX;
      prevMouseY = mousePosY;

      if ( enableMouseLook )
      {
        rotation.x = dy;
        rotation.y = dx;
        rotation *= 0.03f;

        float cameraSpeed = 2.0f;
        if ( keyStates & KEY_W )
        {
          velocity.z = -cameraSpeed;
        }
        if ( keyStates & KEY_S )
        {
          velocity.z = cameraSpeed;
        }
        if ( keyStates & KEY_A )
        {
          velocity.x = -cameraSpeed;
        }
        if ( keyStates & KEY_D )
        {
          velocity.x = cameraSpeed;
        }
      }
      UpdateCamera( &scene.camera, rotation, velocity, logicTimestep );
      t += logicTimestep;
      accumulator -= logicTimestep;
    }

    double interp = accumulator / logicTimestep;
    Render( &scene, interp );
    double mousePosX, mousePosY;
    glfwGetCursorPos( window, &mousePosX, &mousePosY );
    uiNewFrame( mousePosX, mousePosY, currentTime, logicTimestep );
    DisplayMaterialPropertiesWindow( &scene );
    ImGui::Render();
    glfwSwapBuffers( window );
  }
  glfwTerminate();
  return 0;
}
